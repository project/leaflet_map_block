<?php

namespace Drupal\leaflet_map_block\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\leaflet\LeafletService;
use Drupal\leaflet\LeafletSettingsElementsTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "leaflet_map_block",
 *   admin_label = @Translation("Simple map"),
 *   category = @Translation("Leaflet Map Block")
 * )
 */
class SimpleMapBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use LeafletSettingsElementsTrait;

  /**
   * The Default Settings.
   *
   * @var array
   */
  protected $defaultSettings;

  /**
   * Leaflet service.
   *
   * @var \Drupal\Leaflet\LeafletService
   */
  protected $leafletService;

  /**
   * The renderer service.
   *
   * @var \Drupal\core\Render\Renderer
   */
  protected $renderer;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Link generator Service.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $link;

  /**
   * LeafletDefaultFormatter constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Leaflet\LeafletService $leaflet_service
   *   The Leaflet service.
   * @param \Drupal\core\Render\Renderer $renderer
   *   The renderer service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   The Link Generator service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LeafletService $leaflet_service,
    Renderer $renderer,
    ModuleHandlerInterface $module_handler,
    LinkGeneratorInterface $link_generator
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->defaultSettings = self::getDefaultSettings();
    $this->leafletService = $leaflet_service;
    $this->renderer = $renderer;
    $this->moduleHandler = $module_handler;
    $this->link = $link_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('leaflet.service'),
      $container->get('renderer'),
      $container->get('module_handler'),
      $container->get('link_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $settings = ['label_display' => FALSE] + self::getDefaultSettings() + parent::defaultConfiguration();
    $settings['map_position'] = [
      'force' => FALSE,
      'center' => [
        'lat' => '37,2757',
        'lon' => '-6,5',
      ],
    ];
    return $settings;
  }

  /**
   * Get settings.
   *
   * @return array
   *   Settings.
   */
  protected function getSettings() {
    return array_merge($this->defaultConfiguration(), $this->getConfiguration());
  }

  /**
   * Get a setting value.
   *
   * @param string $key
   *   Setting key.
   *
   * @return mixed|null
   *   Setting value.
   */
  public function getSetting($key) {
    $settings = $this->getSettings();
    if (isset($settings[$key])) {
      return $settings[$key];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $form['#tree'] = TRUE;

    $form = parent::blockForm($form, $form_state);
    $field_name = 'simple_block';

    $form['multiple_map'] = [
      '#type' => 'hidden',
      '#value' => 0,
    ];

    $form['popup'] = [
      '#title' => $this->t('Popup Infowindow'),
      '#description' => $this->t('Show a Popup Infowindow on Marker click, with custom content.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['popup'],
    ];

    $form['popup_content'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Popup content'),
      '#description' => $this->t('Define the custom content for the Pop Infowindow. If empty the Content Title will be output.<br>See "REPLACEMENT PATTERNS" above for available replacements.'),
      '#default_value' => $settings['popup_content'],
      '#states' => [
        'visible' => [
          'input[name="fields[' . $field_name . '][settings_edit_form][settings][popup]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Generate the Leaflet Map General Settings.
    $this->generateMapGeneralSettings($form, $settings);

    // Generate the Leaflet Map Reset Control.
    $this->setResetMapControl($form, $settings);

    // Generate the Leaflet Map Position Form Element.
    $map_position_options = $settings['map_position'];
    $form['map_position'] = @$this->generateMapPositionElement($map_position_options);

    $form['map_position']['force']['#default_value'] = TRUE;
    $form['map_position']['force']['#disabled'] = TRUE;

    // Generate the Leaflet Map weight/zIndex Form Element.
    $form['weight'] = $this->generateWeightElement($settings['weight']);

    // Generate Icon form element.
    $icon_options = $settings['icon'];
    $form['icon'] = $this->generateIconFormElement($icon_options);

    // Set Map Marker Cluster Element.
    $this->setMapMarkerclusterElement($form, $settings);

    // Set Map Geometries Options Element.
    $this->setMapPathOptionsElement($form, $settings);

    // Set Map Geocoder Control Element, if the Geocoder Module exists,
    // otherwise output a tip on Geocoder Module Integration.
    $this->setGeocoderMapControl($form, $settings);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['multiple_map'] = $form_state->getValue('multiple_map');
    $this->configuration['popup'] = $form_state->getValue('popup');
    $this->configuration['popup_content'] = $form_state->getValue('popup_content');
    $this->configuration['map_position'] = $form_state->getValue('map_position');
    $this->configuration['weight'] = $form_state->getValue('weight');
    $this->configuration['icon'] = $form_state->getValue('icon');

    $this->configuration['leaflet_map'] = $form_state->getValue('leaflet_map');
    $this->configuration['height'] = $form_state->getValue('height');
    $this->configuration['height_unit'] = $form_state->getValue('height_unit');
    $this->configuration['hide_empty_map'] = $form_state->getValue('hide_empty_map');
    $this->configuration['disable_wheel'] = $form_state->getValue('disable_wheel');
    $this->configuration['fullscreen_control'] = $form_state->getValue('fullscreen_control');
    $this->configuration['gesture_handling'] = $form_state->getValue('gesture_handling');

    $this->configuration['reset_map'] = [
      'control' => $form_state->getValue('reset_map')['control'],
      'position' => $form_state->getValue('reset_map')['position'],
    ];

    $this->configuration['map_position'] = $form_state->getValue('map_position');
    $this->configuration['icon'] = $form_state->getValue('icon');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Sets/consider possibly existing previous Zoom settings.
    $this->setExistingZoomSettings();
    $settings = $this->getSettings();

    // Always render the map, even if we do not have any data.
    $map = leaflet_map_get_info($settings['leaflet_map']);

    // Add a specific map id.
    $map['id'] = Html::getUniqueId("leaflet_map_block_leaflet_map_{$this->getPluginId()}");

    // Get and set the Geofield cardinality.
    $map['geofield_cardinality'] = 1;

    // Set Map additional map Settings.
    $this->setAdditionalMapOptions($map, $settings);

    $results = [];
    $features = [];

    $center = "POINT ({$settings['map_position']['center']['lon']} {$settings['map_position']['center']['lat']})";
    $points = $this->leafletService->leafletProcessGeofield([$center]);
    $feature = $points[0];

    // Generate the weight feature property
    // (falls back to natural result ordering).
    $feature['weight'] = !empty($settings['weight']) ? intval(str_replace([
      "\n",
      "\r",
    ], "", $settings['weight'])) : 0;

    // Eventually set the popup content.
    if ($settings['popup']) {
      // Construct the renderable array for popup title / text. As we later
      // convert that to plain text, losing attachments and cacheability, save
      // them to $results.
      $build = [];
      if ($this->getSetting('popup_content')) {
        $popup_content = $this->getSetting('popup_content');
        $build[] = [
          '#markup' => $popup_content,
        ];
      }

      // We need a string for using it inside the popup. Save attachments and
      // cacheability to $results.
      $render_context = new RenderContext();
      $rendered = $this->renderer->executeInRenderContext($render_context, function () use (&$build) {
        return $this->renderer->render($build, TRUE);
      });
      $feature['popup'] = !empty($rendered) ? $rendered : $settings['label'];
      if (!$render_context->isEmpty()) {
        $render_context->update($results);
      }
    }

    // Add/merge eventual map icon definition from hook_leaflet_map_info.
    if (!empty($map['icon'])) {
      $settings['icon'] = $settings['icon'] ?: [];
      // Remove empty icon options so that they might be replaced by the
      // ones set by the hook_leaflet_map_info.
      foreach ($settings['icon'] as $k => $icon_option) {
        if (empty($icon_option) || (is_array($icon_option) && $this->leafletService->multipleEmpty($icon_option))) {
          unset($settings['icon'][$k]);
        }
      }
      $settings['icon'] = array_replace($map['icon'], $settings['icon']);
    }

    $icon_type = isset($settings['icon']['iconType']) ? $settings['icon']['iconType'] : 'marker';

    // Eventually set the custom Marker icon (DivIcon, Icon Url or
    // Circle Marker).
    if ($feature['type'] === 'point' && isset($settings['icon'])) {

      // Set Feature Icon properties.
      $feature['icon'] = $settings['icon'];

      // Transforms Icon Options that support Replacement Patterns/Tokens.
      if (!empty($settings["icon"]["iconSize"]["x"])) {
        $feature['icon']["iconSize"]["x"] = $settings["icon"]["iconSize"]["x"];
      }
      if (!empty($settings["icon"]["iconSize"]["y"])) {
        $feature['icon']["iconSize"]["y"] = $settings["icon"]["iconSize"]["y"];
      }
      if (!empty($settings["icon"]["shadowSize"]["x"])) {
        $feature['icon']["shadowSize"]["x"] = $settings["icon"]["shadowSize"]["x"];
      }
      if (!empty($settings["icon"]["shadowSize"]["y"])) {
        $feature['icon']["shadowSize"]["y"] = $settings["icon"]["shadowSize"]["y"];
      }

      switch ($icon_type) {
        case 'html':
          $feature['icon']['html'] = $settings['icon']['html'];
          $feature['icon']['html_class'] = isset($settings['icon']['html_class']) ? $settings['icon']['html_class'] : '';
          break;

        case 'circle_marker':
          $feature['icon']['options'] = $settings['icon']['circle_marker_options'];
          break;

        default:
          if (!empty($settings['icon']['iconUrl'])) {
            $feature['icon']['iconUrl'] = str_replace(["\n", "\r"], "", $settings['icon']['iconUrl']);
            if (!empty($feature['icon']['iconUrl'])) {
              // Generate Absolute iconUrl , if not external.
              $feature['icon']['iconUrl'] = $this->leafletService->pathToAbsolute($feature['icon']['iconUrl']);
              // Set the Feature IconSize to the IconUrl Image sizes (if empty).
            }
          }
          if (!empty($settings['icon']['shadowUrl'])) {
            $feature['icon']['shadowUrl'] = str_replace(["\n", "\r"], "", $settings['icon']['shadowUrl']);
            if (!empty($feature['icon']['shadowUrl'])) {
              // Generate Absolute shadowUrl, if not external.
              $feature['icon']['shadowUrl'] = $this->leafletService->pathToAbsolute($feature['icon']['shadowUrl']);
            }
          }
          // Set the Feature IconSize and ShadowSize to the IconUrl or
          // ShadowUrl Image sizes (if empty or invalid).
          $this->leafletService->setFeatureIconSizesIfEmptyOrInvalid($feature);
          break;
      }
    }

    // Associate dynamic path properties (token based) to the feature,
    // in case of not point.
    if ($feature['type'] !== 'point') {
      $feature['path'] = str_replace(["\n", "\r"], "", $settings['path']);
    }

    // Associate dynamic className property (token based) to icon.
    $feature['className'] = !empty($settings['className']) ? str_replace([
      "\n",
      "\r",
    ], "", $settings['className']) : '';

    $features[] = $feature;

    // Order the data features based on the 'weight' element.
    uasort($features, [
      'Drupal\Component\Utility\SortArray',
      'sortByWeightElement',
    ]);

    $js_settings = [
      'map' => $map,
      'features' => $features,
    ];

    // Allow other modules to add/alter the map js settings.
    $items = NULL;
    $this->moduleHandler->alter('leaflet_default_map_formatter', $js_settings, $items);

    $map_height = !empty($settings['height']) ? $settings['height'] . $settings['height_unit'] : '';

    if (!empty($settings['multiple_map'])) {
      foreach ($js_settings['features'] as $k => $feature) {
        $map = $js_settings['map'];
        $map['id'] = $map['id'] . "-{$k}";
        $results['map'] = $this->leafletService->leafletRenderMap($map, [$feature], $map_height);
      }
    }
    // Render the map, if we do have data or the hide option is unchecked.
    elseif (!empty($js_settings['features']) || empty($settings['hide_empty_map'])) {
      $results['map'] = $this->leafletService->leafletRenderMap($js_settings['map'], $js_settings['features'], $map_height);
    }

    $results['extra_info'] = [
      '#markup' => $this->getSetting('popup_content'),
      '#prefix' => '<div class="extra-info">',
      '#suffix' => '</div>',
    ];

    return $results;
  }

  /**
   * Sets possibly existing previous settings for the Zoom Form Element.
   */
  private function setExistingZoomSettings() {
    $settings = $this->getSettings();
    if (isset($settings['zoom'])) {
      $settings['map_position']['zoom'] = (int) $settings['zoom'];
      $settings['map_position']['minZoom'] = (int) $settings['minZoom'];
      $settings['map_position']['maxZoom'] = (int) $settings['maxZoom'];
      $this->configuration['map_position'] = $settings['map_position'];
    }
  }

}
